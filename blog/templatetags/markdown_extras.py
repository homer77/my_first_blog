from django import template
from django.template.defaultfilters import stringfilter

import markdown as md

register = template.Library()


@register.filter()
@stringfilter
def markdown(value):
    return md.markdown(
        value,
        extensions=['markdown.extensions.fenced_code','def_list','sane_lists','footnotes','tables','smarty'],
        extension_configs={
            'smarty' : {'smart_angled_quotes': True}
            })
